﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public void PlayOnBtnclick()
    {
        SceneManager.LoadScene(1);
    }

    public void MainmenuBtnclick()
    {
        SceneManager.LoadScene(0);
    }
    public void ExitBtnclick()
    {
        Application.Quit();
    }
    public void SelectLvl1()
    {
        SceneManager.LoadScene(2);
    }
    public void SelectLvl2()
    {
        SceneManager.LoadScene(3);
    }
}

